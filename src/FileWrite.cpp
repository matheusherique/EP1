#include "FileWrite.hpp"

using namespace std;

FileWrite::FileWrite()
{
	setFileOut(NULL);
}

FileWrite::FileWrite(ofstream fileOut)
{
	setFileOut(fileOut);
}

ofstream FileWrite::getFileOut()
{
	return fileOut;
}

void FileWrite::setFileOut(ofstream fileOut)
{
	this->fileOut = fileOut;
}

void FileWrite::WriteFile(string name)
{
	fileOut.open(name);
	
	fileOut << P5 << endl;
	fileOut << comment << endl;
	fileOut << width << ' ' << height << endl << max << endl;

	for(i = 0; i < width; i++)
	{
		for(j = 0; i < height; j++)
		{
			fileOut.put(pixels[i*width+j]);
		}
	}

	fileOut.close();
}
